#Title: manipulação de worksheets entre workbooks
#Author: nanacoelhopontes
#Date: 15fev2023

import openpyxl as opx
from copy import copy

#Funções desenvolvidas com openpyxl por usuário, disponibilizadas no StackOverflow
# no link: https://stackoverflow.com/questions/42344041/how-to-copy-worksheet-from-one-workbook-to-another-one-using-openpyxl
# pelo usuário Oscar

def copy_sheet(source_sheet, target_sheet):
    copy_cells(source_sheet, target_sheet)  # copy all the cel values and styles
    copy_sheet_attributes(source_sheet, target_sheet)

def copy_sheet_attributes(source_sheet, target_sheet):
    target_sheet.sheet_format = copy(source_sheet.sheet_format)
    target_sheet.sheet_properties = copy(source_sheet.sheet_properties)
    target_sheet.merged_cells = copy(source_sheet.merged_cells)
    target_sheet.page_margins = copy(source_sheet.page_margins)
    target_sheet.freeze_panes = copy(source_sheet.freeze_panes)

    # set row dimensions
    # So you cannot copy the row_dimensions attribute. Does not work (because of meta data in the attribute I think). So we copy every row's row_dimensions. That seems to work.
    for rn in range(len(source_sheet.row_dimensions)):
        target_sheet.row_dimensions[rn] = copy(source_sheet.row_dimensions[rn])

    if source_sheet.sheet_format.defaultColWidth is None:
        print('Unable to copy default column wide')
    else:
        target_sheet.sheet_format.defaultColWidth = copy(source_sheet.sheet_format.defaultColWidth)

    # set specific column width and hidden property
    # we cannot copy the entire column_dimensions attribute so we copy selected attributes
    for key, value in source_sheet.column_dimensions.items():
        target_sheet.column_dimensions[key].min = copy(source_sheet.column_dimensions[key].min)   # Excel actually groups multiple columns under 1 key. Use the min max attribute to also group the columns in the targetSheet
        target_sheet.column_dimensions[key].max = copy(source_sheet.column_dimensions[key].max)  # https://stackoverflow.com/questions/36417278/openpyxl-can-not-read-consecutive-hidden-columns discussed the issue. Note that this is also the case for the width, not onl;y the hidden property
        target_sheet.column_dimensions[key].width = copy(source_sheet.column_dimensions[key].width) # set width for every column
        target_sheet.column_dimensions[key].hidden = copy(source_sheet.column_dimensions[key].hidden)

def copy_cells(source_sheet, target_sheet):
    for (row, col), source_cell in source_sheet._cells.items():
        target_cell = target_sheet.cell(column=col, row=row)

        target_cell._value = source_cell._value
        target_cell.data_type = source_cell.data_type

        if source_cell.has_style:
            target_cell.font = copy(source_cell.font)
            target_cell.border = copy(source_cell.border)
            target_cell.fill = copy(source_cell.fill)
            target_cell.number_format = copy(source_cell.number_format)
            target_cell.protection = copy(source_cell.protection)
            target_cell.alignment = copy(source_cell.alignment)

        if source_cell.hyperlink:
            target_cell._hyperlink = copy(source_cell.hyperlink)

        if source_cell.comment:
            target_cell.comment = copy(source_cell.comment)

# Abertura do arquivo .xlsx onde está um dos arquivos de onde será copiada uma planilha:
arquivoMae = "CMA_Template plano de negocios_2022_Exemplo.xlsx"
workbook = opx.load_workbook(arquivoMae)
source_sheet1 = workbook["Atualização 2022"]

# lista de nomenclatura dos arquivos que serão modificados e formação dos nomes dos arquivos:
listaProprietarios = ["João Leandro", "Luis Antunes Pires", "Francisco Tiago de Oliveira"]
for proprietario in listaProprietarios:
    nomeDestino = "CMA_Plano_de_negocios_" + proprietario + ".xlsx"
    arquivoDestino = opx.load_workbook(nomeDestino)

# captura das planilhas que estão no próprio arquivo a ser modificado
    source_sheet2 = arquivoDestino["5.Financeiro"]
    source_sheet3 = arquivoDestino["Anexo - Fluxo de caixa"]

# criação das planilhas que receberão as cópias
    target_sheet1 = arquivoDestino.create_sheet("Atualização 2022")
    target_sheet2 = arquivoDestino.create_sheet("Financeiro Atualizado")
    target_sheet3 = arquivoDestino.create_sheet("Fluxo de Caixa Atualizado")

# comandos de cópia:
    copy_sheet(source_sheet1,target_sheet1)
    copy_sheet(source_sheet2,target_sheet2)
    copy_sheet(source_sheet3,target_sheet3)

    arquivoDestino.save(nomeDestino)
